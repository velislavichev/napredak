<?php


class FirstCest
{
    public function _before(AcceptanceTester $I)
    {
    }

    public function _after(AcceptanceTester $I)
    {
    }

    // tests
    public function tryToTest(AcceptanceTester $I)
    {   
        $I->amOnPage('/');
        $I->see('Napredak');  
        // $I->fillField('search','Под игото1');
        $I->click(['class' => 'btn btn-primary']);
        $I->see('Под игото1'); 
    }
}
