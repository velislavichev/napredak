<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Book extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $guarded = ['_token', 'user_id'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at', 'taken_at'];

    /**
     * return object
     */
    public function user()
    {
        return $this->hasOne(UserBook::class);
    }

    /**
     * @param $query
     * @param $filters
     */
    public function scopeFilter($query, $filters)
    {
        if ($keyword = $filters['search']) {
            $query->where("name", "LIKE", "%$keyword%")
                ->orWhere("author", "LIKE", "%$keyword%")
                ->orWhere("signature", "LIKE", "%$keyword%");
        }

        if ($filter = $filters['filter']) {
            if ($filter == "available")
                $query->where('available', 1);
            else
                $query->where('available', 0);
        }

        if ($order = $filters['order']) {
            $query->orderBy('created_at', $order);
        } else {
            $query->orderBy('created_at', 'desc');
        }
    }
}
