<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserBook extends Model
{
    use SoftDeletes;

    /**
     * var bool
     */
    public $timestamps = false;


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'book_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function book()
    {
        return $this->belongsTo(Book::class);
    }


    /**
     * @param $query
     * @param $filters
     */
    public function scopeFilter($query, $filters)
    {
        if ($keyword = $filters['search']) {
            $query->whereHas('book', function ($query) use ($keyword) {
                $query->where("name", "LIKE", "%$keyword%");
            });
        }

        // if ($filter = $filters['filter']) {
        //     if ($filter == "available") {
        //         $query->whereHas('book', function ($query) use ($keyword) {
        //             $query->where("available", 1);
        //         });
        //     } else {
        //         $query->whereHas('book', function ($query) use ($keyword) {
        //             $query->where("available", 0);
        //         });
        //     }
        // }

        // if ($order = $filters['order']) {
        //     $query->orderBy('created_at', $order);
        // } else {
        //     $query->orderBy('created_at', 'desc');
        // }
    }
}
