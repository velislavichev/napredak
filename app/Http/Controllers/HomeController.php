<?php

namespace App\Http\Controllers;

use App\Book;   

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $oBooks = Book::filter(request()->all(['search', 'filter', 'order']))->paginate(8);

        return view('welcome', compact('oBooks'));
    }
}
