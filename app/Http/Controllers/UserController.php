<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\UserRequest;
use App\Mail\UserPasswordCreated;
use Illuminate\Support\Facades\Mail;
use App\UserRole;

class UserController extends Controller
{
     /**
     * @var User
     */
    protected $oUser;

    /**
     * UserController constructor.
     * @param User $oUser
     */
    public function __construct(User $oUser)
    {
        $this->middleware('auth');
        $this->middleware('admin');

        $this->oUser = $oUser;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $oUsers = $this->oUser
            ->where('id', '!=', auth()->id())
            ->filter(request()->all(['search', 'filter', 'order']))
            ->paginate(10);

        return view('user.index', compact('oUsers'));
    }

      /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create', ['oUser' => isset($oUser) ? $oUser : []]);
    }

     /**
     * @param UserRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(UserRequest $request)
    {
        $sPassword = substr(md5(microtime()),rand(0,26),6);
        $oUser = $this->oUser->create(array_merge($request->all(), ['password' => bcrypt($sPassword)]));
        $oUser->saveUserRole(new UserRole(), 2);

        Mail::to($oUser)->send(new UserPasswordCreated($sPassword));

        $request->session()->flash('message', __('Successfully created user with name'). ' ' . $oUser->name . '.');
        return redirect('/user');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Book  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('user.edit', ['oUser' => $user]);
    }

    /**
     * @param UserRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(UserRequest $request, $id)
    {
        $oUser = $this->oUser->find($id);
        $oUser->update($request->all());

        $request->session()->flash('message', __('Successfully edited user with name') . ' ' . $oUser->name . '.');
        return redirect('/user');
    }

     /** 
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request, $id)
    {
        $this->oUser->destroy($id);

        $request->session()->flash('message', __('Successfully deleted user') . '.');
        return redirect('/user');
    }
}
