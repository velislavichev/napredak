<?php

namespace App\Http\Controllers;

use App\Book;
use App\Http\Requests\BookRequest;
use Illuminate\Http\Request;
use App\User;
use App\UserBook;

class BookController extends Controller
{
    /**
     * @var Book
     */
    protected $oBook;

    /**
     * BookController constructor.
     * @param Book $oBook
     */
    public function __construct(Book $oBook)
    {
        $this->middleware('auth');
        $this->middleware('admin')->except('userLibrary');
        $this->middleware('user')->only('userLibrary');

        $this->oBook = $oBook;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $oBooks = $this->oBook
            ->filter(request()->all(['search', 'filter', 'order']))
            ->paginate(10);

        return view('book.index', compact('oBooks'));
    }

    /**
     * 
     * @return \Illuminate\Http\Response
     */
    public function userLibrary() {

        $oUserBooks = auth()->user()->book()
            ->filter(request()->all(['search', 'filter', 'order']))
            ->paginate(10);

        return view('user.library.index', compact('oUserBooks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(request()->has('copy')) {
            $oBook = $this->oBook->findOrFail(request('copy'));
        }

        $oUsers = auth()->user()->userRole()->get(['id', 'name']);

        return view('book.create', [
            'oBook' => isset($oBook) ? $oBook : [],
            'oUsers' => $oUsers
        ]);
    }

    /**
     * @param BookRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(BookRequest $request)
    {
        $data = $request->all();
        $data['taken_at'] = !request('available') ? date('Y-m-d H:i:s') : null;

        $oBook = $this->oBook->create($data);

        if(!request('available')) {
            $oBook->user()->updateOrCreate(
                ['book_id' => $oBook->id, 'deleted_at'   => null],
                ['user_id' => request('user_id')
            ]);
        } else {
            $oUserBook = UserBook::where(['book_id' => $oBook->id, 'deleted_at' => null])->first();
            if($oUserBook) {
                $oUserBook->deleted_at = date('Y-m-d H:m:s');
                $oUserBook->save();
            }
        }

        $request->session()->flash('message', __('Successfully created book with name'). ' ' . $oBook->name . '.');
        return redirect('/book');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit(Book $book)
    {
        $oUsers = auth()->user()->userRole()->get(['id', 'name']);

        return view('book.edit', ['oBook' => $book, 'oUsers' => $oUsers]);
    }

    /**
     * @param BookRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(BookRequest $request, $id)
    {
        $data = $request->all();
        $data['taken_at'] = !request('available') ? date('Y-m-d H:i:s') : null;

        $oBook = $this->oBook->find($id);
        $oBook->update($data);
        
        if(!request('available')) {
            $oBook->user()->updateOrCreate(
                ['book_id' => $id, 'deleted_at' => null],
                ['user_id' => request('user_id')
            ]);
        } else {
            $oUserBook = UserBook::where(['book_id' => $id, 'deleted_at' => null])->first();
            if($oUserBook) {
                $oUserBook->deleted_at = date('Y-m-d H:m:s');
                $oUserBook->save();
            }
        }

        $request->session()->flash('message', __('Successfully edited book with name') . ' ' . $oBook->name . '.');
        return redirect('/book');
    }

    /** 
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request, $id)
    {
        $this->oBook->destroy($id);

        $request->session()->flash('message', __('Successfully deleted book') . '.');
        return redirect('/book');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function available($id)
    {
        if (request()->ajax()) {
            try {
                $oBook = $this->oBook->findOrFail($id);
                $oBook->update([
                    'available' => request('available'),
                    'taken_at' => !request('available') ? date('Y-m-d H:i:s') : null,
                ]);

                return response()->json([
                    'success' => $oBook,
                    'message' => __('Successfully edited book with name') . ' '
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    'error' => $e->getMessage(),    
                    'message' => __('Something went wrong') . '.'
                ], 404);
            }
        }
    }
}
