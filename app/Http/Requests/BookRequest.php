<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|string|min:2|unique:books,name,' . request()->route('book') . ',id,deleted_at,NULL',
            'author' => 'required|string|min:2',
            'signature' => 'required|string|min:2',
            'section' => 'required|min:1',
            'number' => 'required|int|digits_between:1,11',
            'available' => 'required',
        ];

        if (!request('available')) {
            $rules = array_merge($rules, [
                'user_id' => 'required',
            ]);
        }

        return $rules;
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
//            'lawyer_id.required' => 'The lawyer field is required.',
//            'comment.required' => 'The comment field required. Information is needed for your lawyer.',
//            'reason.required' => 'The reason field required. Information is needed for the citizen.',
//            'start_date.date_format' => 'The start date does not match the format. Example : 2017-10-19 11:00',
//            'end_date.date_format' => 'The start date does not match the format. Example : 2017-10-19 12:00',
        ];
    }
}
