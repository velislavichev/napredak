<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'last_name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * return object
     */
    public function role()
    {
        return $this->hasOne(UserRole::class);
    }

    /**
     * return object
     */
    public function book()
    {
        return $this->hasMany(UserBook::class);
    }

    /**
     * return user type
     */
    public function isAdmin()
    {
        return $this->role->role_id == 1;
    }

    /**
     * @param UserRole $role
     * @param int      $id
     */
    public function saveUserRole(UserRole $role, $id)
    {
        $role->timestamps = false;
        $role->role_id = $id;

        $this->role()->save($role);
    }

    /**
     * @param $query
     * @param $filters
     */
    public function scopeFilter($query, $filters)
    {
        if ($keyword = $filters['search']) {
            $query->where("name", "LIKE", "%$keyword%")
                ->orWhere("email", "LIKE", "%$keyword%");
        }

        if ($filter = $filters['filter']) {
            if ($filter == "admin")
                $query->whereHas('role', function ($query) use ($keyword) {
                $query->where("role_id", 1);
            });
            else
                $query->whereHas('role', function ($query) use ($keyword) {
                $query->where("role_id", 2);
            });
        }

        if ($order = $filters['order']) {
            $query->orderBy('created_at', $order);
        } else {
            $query->orderBy('created_at', 'desc');
        }
    }

    /**
     * @param $query
     */
    public function scopeUserRole($query)
    {
        $query->whereHas('role', function ($query) {
            $query->where("role_id", 2);
        });
    }
}
