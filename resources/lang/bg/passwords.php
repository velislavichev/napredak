<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Паролата трябва да бъде поне 6 елемента и да съвпада с потвърдената.',
    'reset' => 'Вашата парола беше възстановена!',
    'sent' => 'Изпратихме Ви линк за възстановяване  на паролата!',
    'token' => 'This password reset token is invalid.',
    'user' => "Не намираме такъв потребител.",

];
