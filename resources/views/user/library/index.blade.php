@extends('layouts.app')

@section('content')

    @include('layouts.message')

    <h3>{{ __('Library books') }}</h3>
    
    <div class="row text-center">
        <form action="{{ url('/my-library') }}" method="get">
            <div class="col-md-5 form-group">
                <div class="input-group col-md-12">
                    <input type="text" name="search" value="{{ request('search') }}" class="form-control" placeholder="{{ __('Search by name') }}"/>
                    <input type="hidden" name="order" value="{{ request('order') }}"/>
                    <input type="hidden" name="filter" value="{{ request('filter') }}"/>
                    <span class="input-group-btn">
                        <button class="btn btn-primary" type="submit">
                            {{ __('Search') }}
                        </button>
                    </span>
                </div>
            </div>
        </form>
        {{-- @include('layouts.search', ['filter' => ['available', 'unavailable']]) --}}
        
    </div>

    @if(count($oUserBooks))

        <div class="table-responsive">
            <table class="table" data-form="deleteForm">
                <thead class="thead-inverse">
                <tr>
                    <th width="20%">{{ __('Name') }}</th>
                    <th width="15%">{{ __('Author') }}</th>
                    <th>{{ __('Taken date') }}</th>
                    <th>{{ __('Return date') }}</th>
                    <th>{{ __('Favorite') }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($oUserBooks as $oUserBook)
                    <tr>
                        <td>{{ isset($oUserBook->book->name) ? str_limit($oUserBook->book->name, 20, '...') : '' }}</td>
                        <td>{{ isset($oUserBook->book->author) ? str_limit($oUserBook->book->author, 20, '...' ) : '' }}</td>
                        <td>{{ isset($oUserBook->book->taken_at) ? $oUserBook->book->taken_at->format('d-m-Y') : '' }}</td>
                        <td>{{ isset($oUserBook->book->taken_at) ? ($oUserBook->book->taken_at->modify('+30 day'))->format('d-m-Y') : '' }}</td>
                        <td>
                            <div class="form-group">
                                <div class="form">
                                    <div class="btn-group" data-toggle="buttons">
                                        <input type="checkbox" name="available" class="available" {{ $oUserBook->available ? 'checked' : '' }} data-toggle="toggle" 
                                        data-on="{{ __('Yes') }}" data-off="{{ __('No') }}" data-onstyle="success" data-offstyle="danger">
                                    </div>
                                </div>
                            </div>
                        </td>
                       
                        <input type="hidden" name="bookId" value="{{ $oUserBook->book->id }}">
                    </tr>
                @endforeach
                </tbody>
            </table>

            @include('modals.delete-modal')
        </div>

        {{ $oUserBooks->appends(['order' => request('order'), 'filter' => request('filter')])->links() }}

    @else

        <div class="alert alert-warning">
            {{ __('No books found') }}.
        </div>

    @endif
@endsection
