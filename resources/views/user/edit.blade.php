@extends('layouts.app')

@section('content')

    <h2>{{ __('Update a user') }}</h2>

    <form action="{{ route('user.update', $oUser->id) }}" method="post">
        <input name="_method" type="hidden" value="PUT">
        {{ csrf_field() }}

        <div class="form-group required {{ $errors->has('name') ? 'has-error': '' }}">
            <label class="control-label" for="name">{{ __('Name') }}</label>
            <input id="name" type="text" name="name" class='form-control' value="{{ isset($oUser) ? $oUser->name : old('name') }}">
            <div class="help-block">{{ $errors->first('name') }}</div>
        </div>

        <div class="form-group required {{ $errors->has('last_name') ? 'has-error': '' }}">
            <label class="control-label" for="last_name">{{ __('Last Name') }}</label>
            <input id="last_name" type="text" name="last_name" class='form-control' value="{{ isset($oUser) ? $oUser->last_name : old('last_name') }}">
            <div class="help-block">{{ $errors->first('last_name') }}</div>
        </div>

        <div class="form-group required {{ $errors->has('email') ? 'has-error': '' }}">
            <label class="control-label" for="email">{{ __('Email') }}</label>
            <input id="email" type="text" name="email" class='form-control' value="{{ isset($oUser) ? $oUser->email : old('email') }}">
            <div class="help-block">{{ $errors->first('email') }}</div>
        </div>

        <input type="submit" class='btn btn-primary' value="{{ __('Save') }}">
        <a href="{{ URL::previous() }}" class='btn btn-default'>{{ __('Back') }}</a>

    </form>
    &nbsp

@endsection
