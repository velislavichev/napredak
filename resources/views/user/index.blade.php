@extends('layouts.app')

@section('content')

    @include('layouts.message')

    <h3>{{ __('Users') }}</h3>

    <a href="/user/create" class="btn btn-default" style="margin-bottom:15px;">
        <span class="glyphicon glyphicon-plus"></span>
        {{ __('Create a user') }}
    </a>
    
    <div class="row text-center">
        <form action="{{ url('/user') }}" method="get">
            <div class="col-md-5 form-group">
                <div class="input-group col-md-12">
                    <input type="text" name="search" value="{{ request('search') }}" class="form-control" placeholder="{{ __('Search by name') }}"/>
                    <input type="hidden" name="order" value="{{ request('order') }}"/>
                    <input type="hidden" name="filter" value="{{ request('filter') }}"/>
                    <span class="input-group-btn">
                        <button class="btn btn-primary" type="submit">
                            {{ __('Search') }}
                        </button>
                    </span>
                </div>
            </div>
        </form>
        @include('layouts.search', ['filter' => ['admin', 'user']])
    </div>

    @if(count($oUsers))

        <div class="table-responsive">
            <table class="table" data-form="deleteForm">
                <thead class="thead-inverse">
                <tr>
                    <th>{{ __('Name') }}</th>
                    <th>{{ __('Last Name') }}</th>
                    <th>{{ __('Email') }}</th>
                    <th>{{ __('Role') }}</th>
                    <th>{{ __('Added') }}</th>
                    <th style="width:10%">{{ __('Action') }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($oUsers as $oUser)
                    <tr>
                        <td>{{ isset($oUser->name) ? str_limit($oUser->name, 20, '...') : '' }}</td>
                        <td>{{ isset($oUser->last_name) ? str_limit($oUser->last_name, 20, '...') : '' }}</td>
                        <td>{{ isset($oUser->email) ? str_limit($oUser->email, 20, '...' ): '' }}</td>
                        <td>{{ $oUser->role && $oUser->role->role_id == 1 ? str_limit(__(ucfirst('Admin'))) : str_limit(__(ucfirst('User'))) }}</td>
                        <td>{{ isset($oUser->created_at) ? $oUser->created_at->format('d-m-Y') : '' }}</td>
                        
                        <td>
                            <div style="width:max-content;">     
                                <a href="/user/{{$oUser->id}}/edit" class="btn btn-primary pull-left form-group" style="margin-right: 5px;">
                                    <span class="glyphicon glyphicon-pencil"></span>
                                </a>
                                
                                {{ Form::model($oUser, ['method' => 'delete', 'route' => ['user.destroy', $oUser->id], 'class' => 'pull-right form-delete']) }}
                                {{ Form::hidden('id', $oUser->id) }}
                                {{ Form::button('<span class="glyphicon glyphicon-remove"></span>', ['type' => 'submit', 'class' => 'btn btn-danger'] )  }}
                                {{ Form::close() }}

                            </div>
                        </td>

                    </tr>
                @endforeach
                </tbody>
            </table>

            @include('modals.delete-modal')
        </div>

        {{ $oUsers->appends(['order' => request('order'), 'filter' => request('filter')])->links() }}

    @else

        <div class="alert alert-warning">
            {{ __('No users found') }}.
        </div>

    @endif
@endsection
