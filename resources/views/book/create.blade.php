@extends('layouts.app')

@section('content')

    <h2>{{ request()->has('copy') ? __('Copy a book') : __('Create a book') }}</h2>

    <form action="{{ route('book.store') }}" method="post">
        {{ csrf_field() }}  

        <div class="form-group required {{ $errors->has('name') ? 'has-error': '' }}">
            <label class="control-label" for="name">{{ __('Name') }}</label>
            <input id="name" type="text" name="name" class='form-control' value="{{ !empty($oBook) ? $oBook->name : old('name') }}">
            <div class="help-block">{{ $errors->first('name') }}</div>
        </div>

        <div class="form-group required {{ $errors->has('author') ? 'has-error': '' }}">
            <label class="control-label" for="author">{{ __('Author') }}</label>
            <input id="author" type="text" name="author" class='form-control' value="{{ !empty($oBook) ? $oBook->author : old('author') }}">
            <div class="help-block">{{ $errors->first('author') }}</div>
        </div>

        <div class="form-group required {{ $errors->has('signature') ? 'has-error': '' }}">
            <label class="control-label" for="signature">{{ __('Signature') }}</label>
            <input id="signature" type="text" name="signature" class='form-control' value="{{ !empty($oBook) ? $oBook->signature : old('signature') }}">
            <div class="help-block">{{ $errors->first('signature') }}</div>
        </div>

        <div class="form-group required {{ $errors->has('section') ? 'has-error': '' }}">
            <label class="control-label" for="section">{{ __('Section') }}</label>
            <input id="section" type="text" name="section" class='form-control' value="{{ !empty($oBook) ? $oBook->section : old('section') }}">
            <div class="help-block">{{ $errors->first('section') }}</div>
        </div>

        <div class="form-group required {{ $errors->has('number') ? 'has-error': '' }}">
            <label class="control-label"  for="number">{{ __('Number') }}</label>
            <input id="number" type="text" name="number" class='form-control' value="{{ !empty($oBook) ? $oBook->number : old('number') }}">
            <div class="help-block">{{ $errors->first('number') }}</div>
        </div>

        <div class="form-group">
            <label>{{ __('Available') }}</label>
            <div class="form">
                <div class="btn-group" data-toggle="buttons">
                    <input type='hidden' value='0' name='available'>
                    <input type="checkbox" {{ $errors->first('user_id') ? '' : 'checked'  }} name="available" data-toggle="toggle" value="1"
                    data-on="{{ __('Yes') }}" data-off="{{ __('No') }}" data-onstyle="success" data-offstyle="danger">
                </div>
            </div>
        </div>

        @if($oUsers->count())
            <div class="form-group book-user {{ $errors->first('user_id') ? '' : 'hidden'  }} ui-widget {{ $errors->has('user_id') ? 'has-error': '' }}">
                <label class="control-label"  for="users">{{ __('Users') }}</label>
                <select class="form-control" id="users" name="user_id">
                    <option value=""></option>
                    @foreach ($oUsers as $oUser)
                        <option value="{{ $oUser->id }}">{{ $oUser->name }}</option>
                    @endforeach
                </select>   
                <div class="help-block">{{ $errors->first('user_id') }}</div>
            </div>
        @endif

        <input type="submit" class='btn btn-primary' value="{{ __('Save') }}">
        <a href="{{ URL::previous() }}" class='btn btn-default'>{{ __('Back') }}</a>

    </form>
    &nbsp

@endsection