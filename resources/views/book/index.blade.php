@extends('layouts.app')

@section('content')

    @include('layouts.message')

    <h3>{{ __('Library books') }}</h3>

    <a href="/book/create" class="btn btn-default" style="margin-bottom:15px;">
        <span class="glyphicon glyphicon-plus"></span>
        {{ __('Create a book') }}
    </a>
    
    <div class="row text-center">
        <form action="{{ url('/book') }}" method="get">
            <div class="col-md-5 form-group">
                <div class="input-group col-md-12">
                    <input type="text" name="search" value="{{ request('search') }}" class="form-control" placeholder="{{ __('Search by name') }}"/>
                    <input type="hidden" name="order" value="{{ request('order') }}"/>
                    <input type="hidden" name="filter" value="{{ request('filter') }}"/>
                    <span class="input-group-btn">
                        <button class="btn btn-primary" type="submit">
                            {{ __('Search') }}
                        </button>
                    </span>
                </div>
            </div>
        </form>
        @include('layouts.search', ['filter' => ['available', 'unavailable']])
        
    </div>

    @if(count($oBooks))

        <div class="table-responsive">
            <table class="table" data-form="deleteForm">
                <thead class="thead-inverse">
                <tr>
                    <th width="20%">{{ __('Name') }}</th>
                    <th width="15%">{{ __('Author') }}</th>
                    <th>{{ __('Signature') }}</th>
                    <th>{{ __('Section') }}</th>
                    <th>{{ __('Number') }}</th>
                    <th>{{ __('Return date') }}</th>
                    {{-- <th>{{ __('Added') }}</th> --}}
                    <th>{{ __('Available') }}</th>
                    <th>{{ __('Action') }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($oBooks as $oBook)
                    <tr>
                        <td>{{ isset($oBook->name) ? str_limit($oBook->name, 20, '...') : '' }}</td>
                        <td>{{ isset($oBook->author) ? str_limit($oBook->author, 20, '...' ): '' }}</td>
                        <td>{{ isset($oBook->author) ? str_limit($oBook->signature, 10, '...' ): '' }}</td>
                        <td>{{ isset($oBook->author) ? str_limit($oBook->section, 10, '...' ): '' }}</td>
                        <td>{{ isset($oBook->number) ? $oBook->number : '' }}</td>
                        <td class='taken-date'>{{ isset($oBook->taken_at) ? ($oBook->taken_at->modify('+30 day'))->format('d-m-Y') : 'N/A' }}</td>
                        <td>
                            <div class="form-group">
                                <div class="form">
                                    <div class="btn-group" data-toggle="buttons">
                                        <input type="checkbox" id="toggle-event" name="available" class="available" {{ $oBook->available ? 'checked' : '' }} data-toggle="toggle" 
                                        data-on="{{ __('Yes') }}" data-off="{{ __('No') }}" data-onstyle="success" data-offstyle="danger" data-style="testtoggle">
                                    </div>
                                </div>
                            </div>
                        </td>
                        
                        <td>
                            <div style="width:max-content;">     
                                <a href="/book/{{$oBook->id}}/edit" class="btn btn-primary pull-left form-group" style="margin-right: 5px;">
                                    <span class="glyphicon glyphicon-pencil"></span>
                                </a>
                                
                                <a href="/book/create?copy={{ $oBook->id }}" class="btn btn-info pull-left form-group" style="margin-right: 5px;">
                                    <span class="glyphicon glyphicon-copy"></span>
                                </a>

                                {{ Form::model($oBook, ['method' => 'delete', 'route' => ['book.destroy', $oBook->id], 'class' => 'pull-right form-delete']) }}
                                {{ Form::hidden('id', $oBook->id) }}
                                {{ Form::button('<span class="glyphicon glyphicon-remove"></span>', ['type' => 'submit', 'class' => 'btn btn-danger'] )  }}
                                {{ Form::close() }}

                            </div>
                        </td>

                        <input type="hidden" name="bookId" value="{{ $oBook->id }}">
                    </tr>
                @endforeach
                </tbody>
            </table>

            @include('modals.delete-modal')
        </div>

        {{ $oBooks->appends(['order' => request('order'), 'filter' => request('filter')])->links() }}

    @else

        <div class="alert alert-warning">
            {{ __('No books found') }}.
        </div>

    @endif
@endsection
