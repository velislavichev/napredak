<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4>
                    {{ __('Delete') }} 
                </h4>
            </div>
            <div class="modal-body text-center">
                {{ __('Are you sure you want to continue') . ' ?' }} 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Cancel') }}</button>
                <a class="btn btn-danger btn-ok" id="delete-btn">{{ __('Delete') }}</a>
            </div>
        </div>
    </div>
</div>