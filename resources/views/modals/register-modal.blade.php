<div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                {{ __('Register') }}
            </div>
            <div class="modal-body">
                <form id="register-form" class="form-horizontal" method="POST" action="{{ route('register') }}">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="name" class="col-md-4 control-label">{{ __('Name') }}</label>

                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
                            <small class="help-block"></small>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="last_name" class="col-md-4 control-label">{{ __('Last Name') }}</label>

                        <div class="col-md-6">
                            <input id="last_name" type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" required autofocus>
                            <small class="help-block"></small>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="email-register" class="col-md-4 control-label">{{ __('E-Mail Address') }}</label>

                        <div class="col-md-6">
                            <input id="email-register" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                            <small class="help-block"></small>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="password-register" class="col-md-4 control-label">{{ __('Password') }}</label>

                        <div class="col-md-6">
                            <input id="password-resiter" type="password" class="form-control" name="password" required>
                            <small class="help-block"></small>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="password-confirm" class="col-md-4 control-label">{{ __('Confirm Password') }}</label>

                        <div class="col-md-6">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            <small class="help-block"></small>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Register') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>