@component('mail::message')
#  {{ __('Account Created') }}

 {{ __('You has been successfully registrated by our administrator. Your random generated password is') }} {{ $password }}.

@component('mail::button', ['url' => 'https://napredak-semchinovo.000webhostapp.com/login'])
 {{ __('Login') }}
@endcomponent

 {{ __('Thanks') }},<br>
 {{ __(config('app.name')) }}
@endcomponent