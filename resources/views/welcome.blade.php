@extends('layouts.app')

@section('content')

    <h3>{{ __('Library books') }}</h3>

    <div class="row text-center">
        <form action="{{ url('/') }}" method="get">
            <div class="col-md-5 form-group">
                <div class="input-group col-md-12">
                    <input type="text" name="search" value="{{ request('search') }}"  class="form-control" placeholder="{{ __('Search by name') }}"/>
                    <input type="hidden" name="order" value="{{ request('order') }}"/>
                    <input type="hidden" name="filter" value="{{ request('filter') }}"/>
                    <span class="input-group-btn">
                        <button class="btn btn-primary" type="submit">
                            {{ __('Search') }}
                        </button>
                    </span>
                </div>
            </div>
        </form>
        @include('layouts.search', ['filter' => ['available', 'unavailable']])
    </div>

    @if(count($oBooks))
        <table class="table">
            <thead class="thead-inverse">
            <tr>
                <th>{{ __('Name') }}</th>
                <th>{{ __('Author') }}</th>
                <th>{{ __('Added') }}</th>
                <th>{{ __('Available') }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($oBooks as $oBook)
                <tr>
                    <td>{{ isset($oBook->name) ? str_limit($oBook->name, 20, '...') : '' }}</td>
                    <td>{{ isset($oBook->author) ? str_limit($oBook->author, 20, '...') : '' }}</td>
                    <td>{{ isset($oBook->created_at) ? $oBook->created_at->format('d-m-Y') : '' }}</td>
                    <td>
                        @if($oBook->available == 1 )
                            <label class="btn btn-success disabled">{{ __('Yes') }}</label>
                        @else
                            <label class="btn btn-danger disabled">{{ __('No') }}</label>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <div class="text-center">
            {{ $oBooks->appends(['order' => request('order'), 'filter' => request('filter')])->links() }}
        </div>

    @else

        <div class="alert alert-warning">
            {{ __('No books found') }}.
        </div>

    @endif
@endsection
