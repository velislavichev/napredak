<div class="form-group">
    <select onchange="filter('order', this.value)" class="selectpicker">
        <option value="">{{ __('Order By') }}</option>
        <option {{ request('order') == 'desc' ? 'selected' : '' }} value="desc">{{ __('Latest') }}</option>
        <option {{ request('order') == 'asc' ? 'selected' : '' }} value="asc">{{ __('Oldest') }}</option>
    </select>
    <select onchange="filter('filter', this.value)" class="selectpicker">
        <option value="">{{ __('Filter By') }}</option>
        <option {{ request('filter') == $filter[0] ? 'selected' : '' }} value="{{ $filter[0] }}">
            {{ __(ucfirst($filter[0])) }}
        </option>
        <option {{ request('filter') == $filter[1] ? 'selected' : '' }} value="{{ $filter[1] }}">
            {{ __(ucfirst($filter[1])) }}
        </option>
    </select>
</div>

<script>
    // JS filter function
    function filter(key, value) {
        key = encodeURIComponent(key);
        value = encodeURIComponent(value);

        var kvp = document.location.search.substr(1).split('&');
        if (kvp == '') {
            document.location.search = '?' + key + '=' + value;
        } else {

            var i = kvp.length;
            var x;
            while (i--) {
                x = kvp[i].split('=');

                if (x[0] == key) {
                    x[1] = value;
                    kvp[i] = x.join('=');
                    break;
                }
            }

            if (i < 0) {
                kvp[kvp.length] = [key, value].join('=');
            }

            //this will reload the page, it's likely better to store this until finished
            document.location.search = kvp.join('&');
        }
    };
</script>