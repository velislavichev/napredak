<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="/">
                {{ __('Napredak') }}
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                {{--&nbsp;--}}
            </ul>

            @if (!Auth::guest())

                @if (auth()->user()->role->role_id == 1)
                    <ul class="nav navbar-nav nav-pills">
                        <li class="current {{ request()->is(App::getLocale().'/book') || request()->is(App::getLocale().'/book/*') ? 'active' : '' }}"><a href="{{ url('/book') }}">{{ __('Library') }}</a></li>
                    </ul>

                    <ul class="nav navbar-nav nav-pills">
                        <li class="current {{ request()->is(App::getLocale().'/user') || request()->is(App::getLocale().'/user/*') ? 'active' : '' }}"><a href="{{ url('/user') }}">{{ __('Users') }}</a></li>
                    </ul>
                @else 
                    <ul class="nav navbar-nav nav-pills">
                        <li class="current {{ request()->is(App::getLocale().'/my-library') || request()->is(App::getLocale().'/my-library/*')  ? 'active' : '' }}"><a href="{{ url('/my-library') }}">{{ __('My library') }}</a></li>
                    </ul>
                @endif  
            @endif

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Language dropdown -->
                    
                    @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                        <li>
                            @if($properties['regional'] == 'bg_BG') 
                                <a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                    <img src="{{ asset('img/blank.gif') }}" class="flag flag-bg" alt="Български" />
                                </a>
                            @else
                                <a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                    <img src="{{ asset('img/blank.gif') }}" class="flag flag-gb" alt="English" />
                                </a>
                            @endif
                        </li>
                    @endforeach

                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li>
                            <a href="#" data-href="{{ route('login') }}" data-toggle="modal" data-target="#login">{{ __('Login') }}</a>
                        </li>

                        <li>
                            <a href="#" data-href="{{ route('register') }}" data-toggle="modal" data-target="#register">{{ __('Register') }}</a>
                        </li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                          style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endif
                </ul>
        </div>
    </div>
</nav>
