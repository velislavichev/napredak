
<div id="msg-success" class="alert alert-success" style="display:none"></div>
<div id="msg-error" class="alert alert-danger" style="display:none"></div>
@if (session()->has('message'))
    @if (session()->has('message-info'))
        <div class="alert alert-info fade in">{{ session()->get('message-info') }}</div>
    @endif
    <div class="alert alert-success">{{ session()->get('message') }}</div>
@elseif(session()->has('error'))
    <div class="alert alert-danger fade in">{{ session()->get('error') }}</div>
@endif