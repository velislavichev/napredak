$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
})

var moment = require('moment');

$(function () {
    
    $('.toggle').on("click", function(e) {
        e.preventDefault();
     }).change(function() {
        $('#console-event').bootstrapToggle('toggle');
    })

    // $('.toggle-content').on('click', function (e) {
    //     e.preventDefault;
    //     $(this).siblings('.pop-content').css("display", "block");
    // });

    // $('.close-content').on('click', function (e) {
    //     e.preventDefault;
    //     $(this).closest('.pop-content').css("display", "none");
    // });

    $("input[name='available']").change(function () {
        $('.book-user').toggleClass("hidden");
        
        if ($(this).is(':checked')) {
            $(this).val("1");
        }
    });

    $('#confirm-delete').on('show.bs.modal', function(e) {
        $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
    }); 

    $('table[data-form="deleteForm"]').on('click', '.form-delete', function (e) {
        e.preventDefault();
        var $form = $(this);
        $('#confirm-delete').modal({
                backdrop: 'static',
                keyboard: false
            })
            .on('click', '#delete-btn', function () {
                $form.submit();
            });
    });

    $(".available").on('change', function (e) {
        var id = $("input[name='bookId']", $(this).closest("tr")).val();
        var available = $(this).val();
        var me = $(this);

        if ($(this).is(':checked')) {
            available = 1;
        } else {
            available = 0;
        }

        // if(!available) {
        //     e.preventDefault();
        //     return false;
        // }

        $.ajax({
            method: 'POST',
            url: './available/' + id,
            data: {
                'available': available,
            },
            success: function (response) {
                var taken_at = response['success']['taken_at'];
                if (taken_at == null) {
                    taken_at = "N/A";
                } else {
                    taken_at = moment(taken_at).add(30, 'days').format('DD-M-YYYY');
                }
                
                var name = me.closest('tr').find('.taken-date').html(taken_at);
                $('#msg-success').html(response['message'] + response['success']['name'] + '.').fadeIn('slow');
                $('#msg-success').delay(1500).fadeOut('slow');
            },
            error: function (response, jqXHR, textStatus, errorThrown) {
                $('#msg-error').html(response['responseJSON']['message']).fadeIn('slow');
                $('#msg-error').delay(1500).fadeOut('slow');
                console.log(JSON.stringify(jqXHR));
                console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
            }
        });
    });

    $("#register-form, #login-form").on('submit', function (e) {
        e.preventDefault();

        $('input+small').text('');
        $('input').parent().removeClass('has-error');

        $.ajax({
            method: $(this).attr('method'),
            url: $(this).attr('action'),
            data: $(this).serialize(),
            dataType: "json",
            success: function (response) {
                location.reload();
            },
            error: function (response, jqXHR, textStatus, errorThrown) {
                $.each(response.responseJSON, function (key, value) {
                    var input = '#' + e.target.id + ' input[name=' + key + ']';

                    $(input + '+small').text(value);
                    $(input).parent().addClass('has-error');
                });
            }
        });
    });
    //Navbar
    // $('.current').on('click', function () {
    //     $(this).addClass('active').siblings().removeClass('active');
    // });
});