<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTakenAtColumnToBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('books')) {
            if (!Schema::hasColumn('books', 'taken_at')) {
                Schema::table('books', function (Blueprint $table) {
                    $table->timestamp('taken_at')->after('available')->nullable();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('books')) {
            if (Schema::hasColumn('books', 'taken_at')) {
                Schema::table('books', function (Blueprint $table) {
                    $table->dropColumn('taken_at');
                });
            }
        }
    }
}
