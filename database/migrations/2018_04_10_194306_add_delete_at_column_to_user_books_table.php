<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeleteAtColumnToUserBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('user_books')) {
            if (!Schema::hasColumn('user_books', 'deleted_at')) {
                Schema::table('user_books', function (Blueprint $table) {
                    $table->softDeletes();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('user_books')) {
            if (Schema::hasColumn('user_books', 'deleted_at')) {
                Schema::table('user_books', function (Blueprint $table) {
                    $table->dropSoftDeletes();
                });
            }
        }
    }
}
