<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Book;
use Faker\Generator as Faker;

$factory->define(Book::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence(),
        'author' => $faker->name,
        'signature' => $faker->randomDigit,
        'section' => $faker->randomDigit,
        'number' => $faker->numberBetween(1, 10),
        'available' => true,
    ];
});
